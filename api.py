from flask import request, url_for, send_file, send_from_directory
from flask_api import FlaskAPI, status, exceptions
import os, copy, time

app = FlaskAPI(__name__)
app.config['UPLOAD_FOLDER'] = 'api_upload'

users = {
	1 : {
			"username": "anakarwin",
			"fullname": "Yusuf Rahmatullah",
			"address": "Panglayungan, Bandung",
			"hobbies": ["Reading", "Programming"],
			"imageUrl": "download/user.jpg",
		},
	2 : {
			"username": "reza.irvanda",
			"fullname": "Muhammad Reza Irvanda",
			"address": "Merdeka, Medan",
			"hobbies": ["Programming", "Smack Down"],
			"imageUrl": "download/user.jpg",
		},
	3 : {
			"username": "riva.rave",
			"fullname": "Riva Syafri Rachmatullah",
			"address": "Salatiga, Semarang",
			"hobbies": ["Programming"],
			"imageUrl": "download/user.jpg",
		},
	4 : {
			"username": "taufik.akbar",
			"fullname": "Taufik Akbar Abdullah",
			"address": "Margahayu, Bandung",
			"hobbies": ["Playing Football", "Programming"],
			"imageUrl": "download/user.jpg",
		},
}

notes = {
	0 : "do something",
	1 : "build the codes",
	2 : "paint the door",
}

posts = {
	"1" : {
		"id": 1,
		"title": "Judul Satu",
		"body": "Teks Pertama"
	},
	"2" : {
		"id": 2,
		"title": "Judul Dua",
		"body": "Teks Kedua"
	},
	"3" : {
		"id": 3,
		"title": "Judul Tiga",
		"body": "Teks Ketiga"
	},
	"4" : {
		"id": 4,
		"title": "Judul Empat",
		"body": "Teks Keempat"
	},
	"last": 5,
}

def note_repr(key):
	return {
		'url' : request.host_url.rstrip('/') + url_for('notes_detail', key=key),
		'text' : notes[key],
		'id' : key,
	}

def null_return():
	return {
		"message": 'success',
	}

@app.route("/", methods=['GET', 'POST'])
def notes_list():
	if request.method == 'POST':
		note = str(request.data.get('text', ''))
		idx = max(notes.keys()) + 1
		notes[idx] = note
		return note_repr(idx), status.HTTP_201_CREATED

	return [note_repr(idx) for idx in sorted(notes.keys())]

@app.route("/<int:key>/", methods=['GET', 'PUT', 'DELETE'])
def notes_detail(key):
	if request.method == 'PUT':
		note = str(request.data.get('text', ''))
		notes[key] = note
		return note_repr(key)
	elif request.method == 'DELETE':
		notes.pop(key, None)
		return '', status.HTTP_204_NO_CONTENT

	if key not in notes:
		raise exceptions.NotFound()
	return note_repr(key)

@app.route("/image", methods=['GET'])
def get_image():
	return send_file('capture.png', mimetype='image/gif')

@app.route("/upload", methods=['POST'])
def upload_image():
	print "form: ",request.form
	print "data:",request.data
	print "files:", request.files
	if 'file' not in request.files:
		print ('here')
		# flash('No file part')
		# return redirect(request.url)
		return null_return(), status.HTTP_201_CREATED
	file = request.files['file']
	if file.filename == '':
		print ('or here')
		# flash('No selected file')
		# return redirect(request.url)
		return null_return(), status.HTTP_201_CREATED
	print ('right here')
	filename = file.filename
	final_filename = os.path.join(app.config['UPLOAD_FOLDER'], filename)
	print ('filename: '+filename)
	file.save(final_filename)
	# return redirect(url_for('uploaded_file', filename=filename))
	return null_return(), status.HTTP_201_CREATED

@app.route("/user/<string:username>", methods=['GET'])
def get_user(username):
	for key in users.keys():
		if (users[key]["username"] == username):
			return users[key]
	raise exceptions.NotFound(), 404

@app.route("/download/<path:path>")
def download_image(path):
	return send_from_directory('images',path)

@app.route("/posts", methods=["GET", "POST"])
def createPosts():
	if request.method == "GET":
		retarr = []
		for key in sorted(posts.keys()):
			if key != "last":
				retarr.append(posts[key])
		return retarr
	else:
		title = request.data["title"]
		body = request.data["body"]
		last = posts["last"]
		item = dict()
		item["id"] = str(last)
		item["title"] = title
		item["body"] = body
		posts[str(last)] = item
		posts["last"] += 1
		return posts[str(last)], status.HTTP_201_CREATED


@app.route("/posts/<string:id>", methods=["GET", "PUT", "DELETE"])
def doPost(id):	
	if request.method == "GET":
		if id in posts:
			return posts[id], 200
	elif request.method == "PUT":
		if id in posts:
			title = request.data["title"]
			body = request.data["body"]
			posts[id]["title"] = title
			posts[id]["body"] = body
			return posts[id], status.HTTP_201_CREATED
	elif request.method == "DELETE":
		if id in posts:
			retval = copy.deepcopy(posts[id])
			del posts[id]
			return retval, status.HTTP_204_NO_CONTENT
	return {"id":0, "title":None, "body":None}, 400

if __name__ == '__main__':
	app.run(host='0.0.0.0', debug=True)
 