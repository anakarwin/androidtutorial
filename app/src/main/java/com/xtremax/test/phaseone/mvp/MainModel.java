package com.xtremax.test.phaseone.mvp;

import android.util.Log;

import com.xtremax.test.phaseone.models.Post;

import java.util.ArrayList;

/**
 * Created by E460 on 29/11/2016.
 */

public class MainModel implements MainMVP.ModelOps {

    private MainMVP.RequiredPresenterOps mPresenter;

    private ArrayList<Post> posts;

    public MainModel(MainMVP.RequiredPresenterOps mPresenter) {
        this.mPresenter = mPresenter;
        posts = new ArrayList<>();
    }

    public void insertPost(Post post) {
        posts.add(post);
        mPresenter.onPostInserted(post);
    }

    @Override
    public void removePost(Post post) {
        try {
            posts.remove(post);
        } catch (Exception e) {
            Log.d("debugdebug", "model error: "+e.getMessage());
        }
        mPresenter.onPostRemoved(post);
    }

    @Override
    public void onDestroy() {

    }
}
