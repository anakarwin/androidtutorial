package com.xtremax.test.phaseone.activities;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.xtremax.test.phaseone.R;
import com.xtremax.test.phaseone.models.Post;
import com.xtremax.test.phaseone.network.LocalAPIInterface;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@EActivity(R.layout.activity_try_retrofit)
public class TryRetrofit extends AppCompatActivity implements Callback<Post>{

    @ViewById(R.id.et_id)
    TextInputEditText etId;

    @ViewById(R.id.et_title)
    TextInputEditText etTitle;

    @ViewById(R.id.et_body)
    TextInputEditText etBody;

    @ViewById(R.id.tv_id)
    TextView tvId;

    @ViewById(R.id.tv_title)
    TextView tvTitle;

    @ViewById(R.id.tv_body)
    TextView tvBody;

    @ViewById(R.id.tv_lazy_list)
    TextView tvLazyList;

    @ViewById(R.id.tv_file_uri)
    TextView tvFileUri;

    private static final String URL_BASE = "http://10.0.2.2:5000/";

    private Retrofit mRetrofit;
    private LocalAPIInterface mInterface;
    private ArrayList<Post> posts;
    private OkHttpClient mOkHttpClient;
    private CallbackPostList mListener;
    private CallbackResponseBody mResponseBodyListener;

    @Click(R.id.btn_upload_file)
    void upload() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), 1);
    }

    @Click(R.id.btn_get)
    void getPost() {
        Log.d("debugdebug", "get");
        int id = Integer.valueOf(etId.getText().toString());
        Call<Post> call = mInterface.getPost(id);
        call.enqueue(this);
    }

    @Click(R.id.btn_get_all)
    void getPosts() {
        Log.d("debugdebug", "get all");
        Call<ArrayList<Post>> call = mInterface.getPosts();
        call.enqueue(mListener);
    }

    @Click(R.id.btn_post)
    void createPost() {
        Log.d("debugdebug", "post");
        String title = etTitle.getText().toString();
        String body = etBody.getText().toString();

        Post post = new Post(0, title, body);
        Call<Post> call = mInterface.createPost(post);
        call.enqueue(this);
    }

    @Click(R.id.btn_put)
    void updatePost() {
        Log.d("debugdebug", "put");
        int id = Integer.valueOf(etId.getText().toString());
        String title = etTitle.getText().toString();
        String body = etBody.getText().toString();

        Post post = new Post(id, title, body);
        Call<Post> call = mInterface.updatePost(id, post);
        call.enqueue(this);
    }

    @Click(R.id.btn_delete)
    void deletePost() {
        Log.d("debugdebug", "delete");
        int id = Integer.valueOf(etId.getText().toString());

        Call<Post> call = mInterface.deletePost(id);
        call.enqueue(this);
    }

    @AfterInject
    void prepareAll() {
        mOkHttpClient = new OkHttpClient.Builder()
                .readTimeout(5, TimeUnit.SECONDS)
                .connectTimeout(5, TimeUnit.SECONDS)
                .build();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .client(mOkHttpClient)
                .build();

        mInterface = mRetrofit.create(LocalAPIInterface.class);

        mListener = new CallbackPostList() {

            @Override
            public void onResponseCalled(ArrayList<Post> posts) {
                Log.d("debugdebug", "called");
                setUpPostList(posts);
            }

            @Override
            public void onFailureCalled(Throwable t) {
                Log.d("debugdebug", "error called: "+t.getMessage());
            }
        };
        mResponseBodyListener = new CallbackResponseBody() {
            @Override
            public void onResponseSuccess(ResponseBody responseBody) {
                Log.d("debugdebug", "responseBody: "+responseBody.toString());
                Toast.makeText(getApplicationContext(), "File Uploaded", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponseFailure(Throwable t) {
                Log.d("debugdebug", "rb fail: "+t.getMessage());
            }
        };
    }

    public void updateList() {
        getPosts();
    }

    public void setUpPostList(ArrayList<Post> posts) {
        Log.d("debugdebug", "posts:" + posts.toString());
        StringBuilder lazyString = new StringBuilder();
        for (Post post : posts) {
            lazyString.append(post.getId()).append("\n")
                    .append(post.getTitle()).append("\n")
                    .append(post.getBody()).append("\n")
                    .append("\n");
        }
        tvLazyList.setText(lazyString.toString());
    }

    private void uploadFile(Uri imageUri) {
        Cursor cursor = getContentResolver().query(imageUri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":")+1);
        cursor.close();

        cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " =? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();
        File file = new File(path);
        Log.d("debugdebug", "file: "+file.toString());

        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
        Call<ResponseBody> call = mInterface.uploadFile(body);
        call.enqueue(mResponseBodyListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK && data != null
                && data.getData() != null) {
            Log.d("debugdebug", "data:" + data.toString());
            Uri fileUri = data.getData();
            tvFileUri.setText(fileUri.getPath());
            uploadFile(fileUri);
        }
    }

    @Override
    public void onResponse(Call<Post> call, Response<Post> response) {
        Log.d("debuddebug", "response:"+response.code());
        if (response.code() >= 200 && response.code() < 300) {
            Post post = response.body();
            tvId.setText(String.valueOf(post.getId()));
            tvTitle.setText(post.getTitle());
            tvBody.setText(post.getBody());
            updateList();
        }
    }

    @Override
    public void onFailure(Call<Post> call, Throwable t) {
        Toast.makeText(this, "There is failure:"+t.getMessage(), Toast.LENGTH_SHORT).show();
    }

    public abstract class CallbackPostList implements Callback<ArrayList<Post>> {

        @Override
        public void onResponse(Call<ArrayList<Post>> call, Response<ArrayList<Post>> response) {
            if (response.code() == 200) {
                onResponseCalled(response.body());
            }
        }

        @Override
        public void onFailure(Call<ArrayList<Post>> call, Throwable t) {
            onFailureCalled(t);
        }

        public abstract void onResponseCalled(ArrayList<Post> posts);

        public abstract void onFailureCalled(Throwable t);
    }

    public abstract class CallbackResponseBody implements Callback<ResponseBody> {

        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            onResponseSuccess(response.body());
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {
            onResponseFailure(t);
        }

        public abstract void onResponseSuccess (ResponseBody responseBody);
        public abstract void onResponseFailure (Throwable t);
    }
}
