package com.xtremax.test.phaseone.views;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by E460 on 28/11/2016.
 */

public class ViewWrapper<V extends View> extends RecyclerView.ViewHolder {

    private V view;

    public ViewWrapper(View itemView) {
        super(itemView);
        this.view = (V) itemView;
    }

    public V getView() {
        return view;
    }
}
