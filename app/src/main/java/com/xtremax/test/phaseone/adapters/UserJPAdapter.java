package com.xtremax.test.phaseone.adapters;

import android.content.Context;
import android.view.ViewGroup;

import com.xtremax.test.phaseone.views.UserJPViewHolder;
import com.xtremax.test.phaseone.views.UserJPViewHolder_;
import com.xtremax.test.phaseone.views.ViewWrapper;
import com.xtremax.test.phaseone.models.UserJsonPlaceholder;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;

/**
 * Created by E460 on 28/11/2016.
 */

@EBean
public class UserJPAdapter extends RecyclerViewAdapterBase<UserJsonPlaceholder, UserJPViewHolder> {


    private Context context;

    public static UserJPAdapter newInstance(ArrayList<UserJsonPlaceholder> users, Context context) {
        UserJPAdapter adapter = new UserJPAdapter(context);
        adapter.items = users;
        return adapter;
    }

    protected UserJPAdapter(Context context) {
        this.context = context;
    }

    @Override
    protected UserJPViewHolder onCreateItemView(ViewGroup parent, int viewType) {
        return UserJPViewHolder_.build(context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<UserJPViewHolder> holder, int position) {
        UserJPViewHolder view = holder.getView();
        UserJsonPlaceholder user = items.get(position);
        view.bind(user);
    }
}
