package com.xtremax.test.phaseone.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.xtremax.test.phaseone.R;

public class FacebookLogin extends AppCompatActivity {

    private LoginButton btnLogin;
    private TextView tvReport;

    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        setContentView(R.layout.activity_facebook_login);

        btnLogin = (LoginButton) findViewById(R.id.btn_fb_login);
        tvReport = (TextView) findViewById(R.id.tv_report);

        btnLogin.setReadPermissions("email");
        callbackManager = CallbackManager.Factory.create();
        btnLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                tvReport.setText("login success");
            }

            @Override
            public void onCancel() {
                tvReport.setText("login canceled");
            }

            @Override
            public void onError(FacebookException error) {
                tvReport.setText("login error cause: "+error.getMessage());
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AccessToken.getCurrentAccessToken() != null) {  // logging out
                    tvReport.setText("");
                }
            }
        });

        if (AccessToken.getCurrentAccessToken() != null
        && !AccessToken.getCurrentAccessToken().isExpired() && Profile.getCurrentProfile() != null) {
            tvReport.setText("Logged as: " + Profile.getCurrentProfile().getName());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
