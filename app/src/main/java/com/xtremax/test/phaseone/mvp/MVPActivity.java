package com.xtremax.test.phaseone.mvp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.xtremax.test.phaseone.R;

import java.lang.ref.WeakReference;

public class MVPActivity extends AppCompatActivity implements MainMVP.RequiredViewOps{

    protected final String TAG = getClass().getSimpleName();

    private final StateMaintainer mStateMaintainer =
            new StateMaintainer(TAG, new WeakReference<FragmentManager>(this.getSupportFragmentManager()));

    private MainMVP.PresenterOps mPresenter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startMVPOps();
        setContentView(R.layout.activity_mvp);
    }

    public void startMVPOps() {
        try{
            if (mStateMaintainer.firstTimeIn()) {
                Log.d(TAG, "onCreate() called for the first time");
                initialize(this);
            } else {
                Log.d(TAG, "onCreate() called more than once");
                reintialize(this);
            }
        }catch (IllegalAccessException e) {
            Log.d(TAG, "onCreate(): " + e);
            throw new RuntimeException(e);
        }
    }

    private void reintialize(MainMVP.RequiredViewOps view) throws IllegalAccessException {
        mPresenter = mStateMaintainer.get(MainMVP.PresenterOps.class.getSimpleName());
        if (mPresenter == null) {
            Log.w(TAG, "recreating Presenter");
            initialize(view);
        } else {
            mPresenter.onConfigurationChanged(view);
        }
    }

    private void initialize(MainMVP.RequiredViewOps view) throws IllegalAccessException{
        mPresenter = new MainPresenter(new WeakReference<MainMVP.RequiredViewOps>(view));
        mStateMaintainer.put(MainMVP.PresenterOps.class.getSimpleName(), mPresenter);
    }

    @Override
    public void showToast(String msg) {
        Toast.makeText(this,"err:" + msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showAlert(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
