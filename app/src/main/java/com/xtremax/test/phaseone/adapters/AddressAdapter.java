package com.xtremax.test.phaseone.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xtremax.test.phaseone.R;
import com.xtremax.test.phaseone.models.User;

import java.util.List;

/**
 * Created by E460 on 22/11/2016.
 */

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.AddressViewHolder>{

    private List<User> users;
    private Context context;

    public AddressAdapter(List<User> users, Context context) {
        this.users = users;
        this.context = context;
    }

    @Override
    public AddressViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.item_address, parent, false);
        return new AddressViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AddressViewHolder holder, int position) {
        User user = users.get(position);
        holder.tvFullname.setText(user.getFullname());
        holder.tvAddress.setText(user.getAddress());
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    class AddressViewHolder extends RecyclerView.ViewHolder {
        TextView tvFullname;
        TextView tvAddress;

        public AddressViewHolder(View itemView) {
            super(itemView);
            tvFullname = (TextView) itemView.findViewById(R.id.tv_fullname);
            tvAddress = (TextView) itemView.findViewById(R.id.tv_address);
        }
    }
}
