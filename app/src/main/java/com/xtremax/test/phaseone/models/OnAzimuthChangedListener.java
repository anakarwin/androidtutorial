package com.xtremax.test.phaseone.models;

/**
 * Created by E460 on 30/11/2016.
 */

public interface OnAzimuthChangedListener {

    void onAzimuthChanged(float azimuthFrom, float azimuthTo);
}
