package com.xtremax.test.phaseone.activities;

import android.content.pm.ActivityInfo;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.xtremax.test.phaseone.R;
import com.xtremax.test.phaseone.models.AugmentedPOI;
import com.xtremax.test.phaseone.models.MyCurrentAzimuth;
import com.xtremax.test.phaseone.models.MyCurrentLocation;
import com.xtremax.test.phaseone.models.OnAzimuthChangedListener;
import com.xtremax.test.phaseone.models.OnLocationChangedListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CameraViewActivity extends AppCompatActivity implements SurfaceHolder.Callback,
        OnAzimuthChangedListener, OnLocationChangedListener{

    private SurfaceHolder mSurfaceHolder;
    private AugmentedPOI mPOI;
    private MyCurrentAzimuth myCurrentAzimuth;
    private MyCurrentLocation myCurrentLocation;
    private Camera camera;

    private boolean isCameraViewOn = false;
    private double mAzimuthReal = 0;
    private double mAzimuthTeoritical = 0;
    private double mLatitude = 0;
    private double mLongitude = 0;

    private static double AZIMUYH_ACCURACY = 5;

    TextView tvDescription;
    ImageView ivPointerIcon;
    SurfaceView svCamera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_view);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ivPointerIcon = (ImageView) findViewById(R.id.iv_pointer_icon);
        tvDescription = (TextView) findViewById(R.id.tv_description);
        svCamera = (SurfaceView) findViewById(R.id.sv_camera);

        getWindow().setFormat(PixelFormat.UNKNOWN);
        mSurfaceHolder = svCamera.getHolder();
        mSurfaceHolder.addCallback(this);

        setupListeners();
        setAugmentedRealityPoint();
    }

    private void setupListeners() {
        myCurrentLocation = new MyCurrentLocation(this, this);
        myCurrentLocation.buildGoogleApiCLient(this);
        myCurrentLocation.start();

        myCurrentAzimuth = new MyCurrentAzimuth(this, this);
        myCurrentAzimuth.start();
    }

    /**
     * dummy POI
     */
    private void setAugmentedRealityPoint() {
        mPOI = new AugmentedPOI("Xtremax Bandung", "Xtremax Bandung", -6.8975909, 107.5950409);
    }

    private double calculateTeoriticalAzimuth() {
        double dx = mPOI.getLatitude() - mLatitude;
        double dy = mPOI.getLongitude() - mLatitude;
        double phiAngle = Math.toDegrees(Math.atan(Math.abs(dy / dx)));

        if (dx > 0 && dy > 0) {
            return phiAngle;
        } else if (dx < 0 && dy > 0) {
            return 180 - phiAngle;
        } else if (dx < 0 && dy < 0) {
            return 180 + phiAngle;
        } else if (dx > 0 && dy < 0) {
            return 360 - phiAngle;
        }

        return phiAngle;
    }

    private List<Double> calculateAzimuthAccuracy(double azimuth) {
        double minAngle = azimuth - AZIMUYH_ACCURACY;
        double maxAngle = azimuth + AZIMUYH_ACCURACY;
        List<Double> minMax = new ArrayList<Double>();

        if (minAngle < 0) {
            minAngle += 360;
        }

        if (maxAngle >= 360) {
            maxAngle -= 360;
        }

        minMax.add(minAngle);
        minMax.add(maxAngle);

        return minMax;
    }

    private boolean isBetween(double minAngle, double maxAngle, double azimuth) {
        if (minAngle > maxAngle) {
            return (isBetween(0, maxAngle, azimuth) && isBetween(minAngle, 360, azimuth));
        } else {
            return (azimuth > minAngle && azimuth < maxAngle);
        }
    }

    private void updateDescription() {
        tvDescription.setText(mPOI.getName() +
                " azimuthTeoritical " + mAzimuthTeoritical +
                " azimuthReal " + mAzimuthReal +
                " posititon: " + mLatitude + ", " + mLongitude);
    }


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        camera = Camera.open();
        camera.setDisplayOrientation(90);
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {
        if (isCameraViewOn) {
            camera.stopPreview();
            isCameraViewOn = false;
        }

        if (camera != null) {
            try {
                camera.setPreviewDisplay(surfaceHolder);
                camera.startPreview();
                isCameraViewOn = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        camera.stopPreview();
        camera.release();
        camera = null;
        isCameraViewOn = false;
    }

    @Override
    public void onLocationChanged(Location location) {
        mLatitude = location.getLatitude();
        mLongitude = location.getLongitude();
        mAzimuthTeoritical = calculateTeoriticalAzimuth();
        updateDescription();
    }

    @Override
    public void onAzimuthChanged(float azimuthFrom, float azimuthTo) {
        mAzimuthReal = azimuthTo;
        mAzimuthTeoritical = calculateTeoriticalAzimuth();

        List<Double> minMax = calculateAzimuthAccuracy(mAzimuthTeoritical);
        if (isBetween(minMax.get(0), minMax.get(1), mAzimuthReal)) {
            ivPointerIcon.setVisibility(View.VISIBLE);
        } else {
            ivPointerIcon.setVisibility(View.INVISIBLE);
        }

        updateDescription();
    }

    @Override
    protected void onStop() {
        myCurrentAzimuth.stop();
        myCurrentLocation.stop();
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        myCurrentLocation.start();
        myCurrentAzimuth.start();
    }
}
