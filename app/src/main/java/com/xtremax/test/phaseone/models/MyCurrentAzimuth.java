package com.xtremax.test.phaseone.models;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Created by E460 on 30/11/2016.
 */

public class MyCurrentAzimuth implements SensorEventListener {

    private SensorManager sensorManager;
    private Sensor sensor;
    private int azimuthFrom = 0;
    private int azimuthTo = 0;
    private OnAzimuthChangedListener azimuthChangedListener;
    Context context;

    public MyCurrentAzimuth(OnAzimuthChangedListener azimuthChangedListener, Context context) {
        this.azimuthChangedListener = azimuthChangedListener;
        this.context = context;
    }

    public void start() {
        sensorManager = (SensorManager) context.getSystemService(context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_UI);
    }

    public void stop() {
        sensorManager.unregisterListener(this);
    }

    public void setAzimuthChangedListener(OnAzimuthChangedListener azimuthChangedListener) {
        this.azimuthChangedListener = azimuthChangedListener;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        azimuthFrom = azimuthTo;

        float[] orientation = new float[3];
        float[] rMat = new float[9];
        SensorManager.getRotationMatrixFromVector(rMat, sensorEvent.values);
        azimuthTo = (int) (Math.toDegrees(SensorManager.getOrientation(rMat, orientation)[0]) + 360) % 360;

        azimuthChangedListener.onAzimuthChanged(azimuthFrom, azimuthTo);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
