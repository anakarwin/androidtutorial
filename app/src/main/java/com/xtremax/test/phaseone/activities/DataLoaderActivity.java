package com.xtremax.test.phaseone.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.content.SharedPreferencesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.xtremax.test.phaseone.R;

public class DataLoaderActivity extends AppCompatActivity {

    private TextView tvText;
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_loader);

        tvText = (TextView) findViewById(R.id.tv_text);
        mSharedPreferences = getSharedPreferences(DataSaveActivity.OWN_PREFERENCES,
                Context.MODE_PRIVATE);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(mSharedPreferences.getString("username", "")+"\n");
        stringBuilder.append(mSharedPreferences.getString("password", "")+"\n");
        tvText.setText(stringBuilder.toString());
    }
}
