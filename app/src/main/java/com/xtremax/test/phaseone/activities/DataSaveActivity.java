package com.xtremax.test.phaseone.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.xtremax.test.phaseone.R;

import java.util.Set;

public class DataSaveActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnSubmit;
    private TextInputEditText etUsername;
    private TextInputEditText etPassword;
    private TextInputEditText etConfirmPassword;

    private SharedPreferences mSharedPreferences;

    public static final String OWN_PREFERENCES = "MyPreferences";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_save);

        btnSubmit = (Button) findViewById(R.id.btn_submit);
        etUsername = (TextInputEditText) findViewById(R.id.et_username);
        etPassword = (TextInputEditText) findViewById(R.id.et_password);
        etConfirmPassword = (TextInputEditText) findViewById(R.id.et_confirm_password);

        btnSubmit.setOnClickListener(this);

        mSharedPreferences = getSharedPreferences(OWN_PREFERENCES,
                Context.MODE_PRIVATE | Context.MODE_APPEND);
    }

    @Override
    public void onClick(View view) {
        if (isFormValidate()) {
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putString("username", etUsername.getText().toString());
            editor.putString("password", etPassword.getText().toString());
            editor.commit();
            Intent intent = new Intent(this, DataLoaderActivity.class);
            startActivity(intent);
        }
    }

    private boolean isFormValidate() {
        String username = etUsername.getText().toString();
        if (username.isEmpty()) {
            ((TextInputLayout) etUsername.getParent()).setError("Username must be filled");
//            etUsername.setError("Username must be filled");
            return false;
        } else if (username.length() < 4) {
            etUsername.setError("Username must 4 letters or more");
            return false;
        }

        String password = etPassword.getText().toString();
        String confirm = etConfirmPassword.getText().toString();
        if (password.isEmpty()) {
            etPassword.setError("Password must be filled");
            return false;
        } else if (password.length() < 8) {
            etPassword.setError("Password must 8 character or more");
            return false;
        }
        if (!confirm.equals(password)) {
            etConfirmPassword.setError("Password must be same");
            return false;
        }

        return true;
    }
}
