package com.xtremax.test.phaseone.helper;

import com.xtremax.test.phaseone.models.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by E460 on 22/11/2016.
 */
public class UserHelper {

    private static String[] hobbies = {"Swimming", "Playing footbal", "Boxing", "Reading"};
    private static String[] skills = {"Programming", "Presenting", "Persuasing"};

    public static final String ARG_USERS = "arg_users";

    private static UserHelper ourInstance = new UserHelper();

    public static UserHelper getInstance() {
        return ourInstance;
    }

    private UserHelper() {
    }

    public static List<User> generateData() {
        List<User> users = new ArrayList<User>();
        users.add(new User(1, "anakarwin", "Yusuf Rahmatullah", "Panglayungan, Bandung",
                Arrays.asList(hobbies), Arrays.asList(skills)));
        users.add(new User(2, "reza.irvanda", "Muhammad reza Irvanda", "Merdeka, Medan",
                Arrays.asList(Arrays.copyOfRange(hobbies, 0, 2)), Arrays.asList(skills)));
        users.add(new User(3, "menori", "Marcellinus Henry Menory", "Cimahi",
                Arrays.asList(hobbies), Arrays.asList(Arrays.copyOfRange(skills, 1, 2))));
        users.add(new User(4, "riva.syafri", "Riva Syafri Rachmatullah", "Salatiga, Semarang",
                Arrays.asList(Arrays.copyOfRange(hobbies, 1, 3)), Arrays.asList(skills)));
        users.add(new User(5, "ghani.ruhman", "Ghani RUhman", "Bojong Koneng, Bandung",
                Arrays.asList(Arrays.copyOfRange(hobbies, 1, 3)), Arrays.asList(skills)));
        users.add(new User(6, "taufikaa", "Taufik Akbar Abdullah", "Bandung",
                Arrays.asList(Arrays.copyOfRange(hobbies, 1, 3)), Arrays.asList(skills)));
        users.add(new User(7, "nimasputri", "Nimas Putri Astriningtyas", "Tegal",
                Arrays.asList(Arrays.copyOfRange(hobbies, 1, 3)), Arrays.asList(skills)));

        return users;
    }
}
