package com.xtremax.test.phaseone.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xtremax.test.phaseone.R;
import com.xtremax.test.phaseone.adapters.UserJPAdapter;
import com.xtremax.test.phaseone.models.UserJsonPlaceholder;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.ViewsById;

import java.util.ArrayList;
import java.util.List;

@EFragment(R.layout.fragment_user_json_placeholder)
public class UserJsonPlaceholderFragment extends Fragment {

    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @ViewById(R.id.rv_container)
    RecyclerView mRecyclerView;

    @FragmentArg
    ArrayList<UserJsonPlaceholder> users;

    @AfterViews
    protected void setViews() {
        Log.d("debugdebug", "[Fragment] users: "+users);
        if (null != users && users.size() > 0) {
            mAdapter = UserJPAdapter.newInstance(users, getContext());
            mRecyclerView.setAdapter(mAdapter);
            mLayoutManager = new LinearLayoutManager(getContext(),
                    LinearLayoutManager.VERTICAL, false);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.addItemDecoration(
                    new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        }
    }
}
