package com.xtremax.test.phaseone.network;

import com.xtremax.test.phaseone.models.UserJsonPlaceholder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by E460 on 25/11/2016.
 */

public interface UserJPInterface {
    @GET("{id}")
    Call<UserJsonPlaceholder> getUser(@Path("id") int id);

    @GET("users")
    Call<ArrayList<UserJsonPlaceholder>> getUsers();
}
