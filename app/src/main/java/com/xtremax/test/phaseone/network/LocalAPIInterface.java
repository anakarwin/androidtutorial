package com.xtremax.test.phaseone.network;

import com.xtremax.test.phaseone.models.Post;

import java.lang.reflect.Array;
import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by E460 on 28/11/2016.
 */

public interface LocalAPIInterface {

    @GET("posts")
    Call<ArrayList<Post>> getPosts();

    @POST("posts")
    Call<Post> createPost(@Body Post post);

    @GET("posts/{id}")
    Call<Post> getPost(@Path("id") int id);

    @PUT("posts/{id}")
    Call<Post> updatePost(@Path("id") int id, @Body Post post);

    @DELETE("posts/{id}")
    Call<Post> deletePost(@Path("id") int id);

    @Multipart
    @POST("upload")
    Call<ResponseBody> uploadFile(@Part MultipartBody.Part file);

}
