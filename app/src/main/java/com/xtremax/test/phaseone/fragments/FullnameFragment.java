package com.xtremax.test.phaseone.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.xtremax.test.phaseone.R;
import com.xtremax.test.phaseone.adapters.FullnameAdapter;
import com.xtremax.test.phaseone.helper.UserHelper;
import com.xtremax.test.phaseone.models.User;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FullnameFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FullnameFragment extends Fragment {

    private List<User> users;

    public FullnameFragment() {
        // Required empty public constructor
    }

    public static FullnameFragment newInstance(List<User> users) {

        Bundle args = new Bundle();
        FullnameFragment fragment = new FullnameFragment();
        args.putParcelableArrayList(UserHelper.ARG_USERS, (ArrayList) users);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            users = getArguments().getParcelableArrayList(UserHelper.ARG_USERS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fullname, container, false);;
        GridView gv = (GridView) view.findViewById(R.id.gv_container);
        FullnameAdapter adapter = new FullnameAdapter(users, getContext());
        gv.setAdapter(adapter);
        return view;
    }

}
