package com.xtremax.test.phaseone.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xtremax.test.phaseone.R;
import com.xtremax.test.phaseone.adapters.UserDataAdapter;
import com.xtremax.test.phaseone.helper.UserHelper;
import com.xtremax.test.phaseone.models.User;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserDataFragment extends Fragment {

    private List<User> mUsers;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.ItemDecoration vItemDecoration;
    private RecyclerView.ItemDecoration hItemDecoration;
    private RecyclerView.LayoutManager mLayoutManager;

    public UserDataFragment() {
        // Required empty public constructor
    }

    public static UserDataFragment newInstance(List<User> users) {

        Bundle args = new Bundle();

        UserDataFragment fragment = new UserDataFragment();
        args.putParcelableArrayList(UserHelper.ARG_USERS, (ArrayList) users);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (null != getArguments()) {
            mUsers = getArguments().getParcelableArrayList(UserHelper.ARG_USERS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_data, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_container);
        mAdapter = new UserDataAdapter(mUsers);
        mLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        vItemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        hItemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
//        mRecyclerView.addItemDecoration(vItemDecoration);
//        mRecyclerView.addItemDecoration(hItemDecoration);

        return view;
    }

}
