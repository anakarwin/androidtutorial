package com.xtremax.test.phaseone.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xtremax.test.phaseone.R;
import com.xtremax.test.phaseone.models.User;

import org.w3c.dom.Text;

import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by E460 on 22/11/2016.
 */

public class UserDataAdapter extends RecyclerView.Adapter<UserDataAdapter.UserDataViewHolder> {

    private List<User> mUsers;

    public UserDataAdapter(List<User> mUsers) {
        this.mUsers = mUsers;
    }

    @Override
    public UserDataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_userdata, parent, false);
        return new UserDataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserDataViewHolder holder, int position) {
        User user = mUsers.get(position);
        StringBuilder hobbies = new StringBuilder();
        StringBuilder skills = new StringBuilder();

        holder.tvId.setText(String.valueOf(user.getId()));
        holder.tvUsername.setText(user.getUsername());
        holder.tvFullname.setText(user.getFullname());
        holder.tvAddress.setText(user.getAddress());

        hobbies.append("Hobbies:\n");
        for (int i = 0; i < user.getHobbies().size(); i++) {
            hobbies.append(user.getHobbies().get(i) + "\n");
        }
        hobbies.deleteCharAt(hobbies.length()-1);
        holder.tvHobbies.setText(hobbies.toString());

        skills.append("Skills:\n");
        for (int i = 0; i < user.getSkills().size(); i++) {
            skills.append(user.getSkills().get(i) + "\n");
        }
        skills.deleteCharAt(skills.length()-1);
        holder.tvSkills.setText(skills.toString());
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    class UserDataViewHolder extends RecyclerView.ViewHolder {
        TextView tvId;
        TextView tvUsername;
        TextView tvFullname;
        TextView tvAddress;
        TextView tvHobbies;
        TextView tvSkills;
        public UserDataViewHolder(View itemView) {
            super(itemView);
            tvId = (TextView) itemView.findViewById(R.id.tv_id);
            tvUsername = (TextView) itemView.findViewById(R.id.tv_username);
            tvFullname = (TextView) itemView.findViewById(R.id.tv_fullname);
            tvAddress = (TextView) itemView.findViewById(R.id.tv_address);
            tvHobbies = (TextView) itemView.findViewById(R.id.tv_hobbies);
            tvSkills = (TextView) itemView.findViewById(R.id.tv_skills);
        }
    }
}
