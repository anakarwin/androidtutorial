package com.xtremax.test.phaseone.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.xtremax.test.phaseone.R;
import com.xtremax.test.phaseone.models.User;

import java.util.List;

/**
 * Created by E460 on 22/11/2016.
 */

public class UsernameAdapter extends ArrayAdapter<User> {
    public UsernameAdapter(Context context, int resource, List<User> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        User user = getItem(position);

        if (null == convertView) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.item_username, parent, false);
        }

        TextView tvId = (TextView) convertView.findViewById(R.id.tv_id);
        TextView tvUsername = (TextView) convertView.findViewById(R.id.tv_username);
        tvId.setText(String.valueOf(user.getId()));
        tvUsername.setText(user.getUsername());
        return convertView;
    }
}
