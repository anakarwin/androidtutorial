package com.xtremax.test.phaseone.models;

import android.location.Location;

/**
 * Created by E460 on 30/11/2016.
 */

public interface OnLocationChangedListener {

    void onLocationChanged(Location currentLocation);
}
