package com.xtremax.test.phaseone.network;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.xtremax.test.phaseone.models.Note;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by E460 on 22/11/2016.
 */

public class APIConnector {

    private static final String URL_IMAGE = "http://10.0.2.2:5000/image";
    private static final String URL_NOTE = "http://10.0.2.2:5000/";
    private static final String URL_UPLOAD = "http://10.0.2.2:5000/upload";
    private static final String URL_BASE = "http://10.0.2.2:5000/user/";

    public static final int BUFFER_SIZE = 1024;

    public static Bitmap downloadImage() {
        InputStream is = null;
        try {
            URL url = new URL(URL_IMAGE);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();

            int response = conn.getResponseCode();
            Log.d("debugdebug", "response: "+response);
            is = conn.getInputStream();
            return BitmapFactory.decodeStream(is);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Note downloadNote(int index) {
        InputStream is = null;
        try {
            URL url = new URL(URL_NOTE+index);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();

            int response = conn.getResponseCode();
            if (404 == response) {
                return null;
            } else {
                is = conn.getInputStream();
                Reader reader = new InputStreamReader(is, "UTF-8");
                char[] buffer = new char[BUFFER_SIZE];
                StringBuilder content = new StringBuilder();
                for(;;) {
                    int rsz = reader.read(buffer, 0, BUFFER_SIZE);
                    if (rsz < 0) {
                        break;
                    }
                    content.append(buffer, 0, rsz);
                }
                return Note.fromJson(content.toString());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return new Note();
    }

    public static void uploadFile(String filePath) {
        String boundary = "*****";
        String crlf = "\r\n";
        String twoHyp = "--";

        HttpURLConnection conn;
        DataOutputStream dos;
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        File sourceFile = new File(filePath);
        try {
            // setting the network
            URL url = new URL(URL_UPLOAD);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Cache-Control", "no-cache");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);

            // start content wrapper
            dos = new DataOutputStream(conn.getOutputStream());
            dos.writeBytes(twoHyp + boundary + crlf);
            dos.writeBytes("Content-Disposition: form-data; name=\"file\";filename=\"" + filePath
                    + "\"" + crlf);
            dos.writeBytes(crlf);

            // convert to bytes
            FileInputStream fis = new FileInputStream(filePath);
            bytesAvailable = fis.available();
            bufferSize = Math.min(bytesAvailable, BUFFER_SIZE);
            buffer = new byte[bufferSize];
            bytesRead = fis.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fis.available();
                bufferSize = Math.min(bytesAvailable, BUFFER_SIZE);
                bytesRead = fis.read(buffer, 0, bufferSize);
            }
            dos.writeBytes(crlf);
            dos.writeBytes(twoHyp+boundary+twoHyp+crlf);
            fis.close();
            dos.flush();
            dos.close();
            conn.disconnect();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
