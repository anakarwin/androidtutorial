package com.xtremax.test.phaseone.adapters;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xtremax.test.phaseone.R;
import com.xtremax.test.phaseone.models.User;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by E460 on 22/11/2016.
 */

public class FullnameAdapter extends BaseAdapter {

    private List<User> users;
    private Context context;

    public FullnameAdapter(List<User> users, Context context) {
        this.users = users;
        this.context = context;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int i) {
        return users.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        User user = users.get(i);
        if (null == view) {
            view = LayoutInflater.from(context).inflate(R.layout.item_fullname, viewGroup, false);
        }
        TextView tvId = (TextView) view.findViewById(R.id.tv_id);
        TextView tvFullname = (TextView) view.findViewById(R.id.tv_fullname);
        tvId.setText(String.valueOf(user.getId()));
        tvFullname.setText(user.getFullname());
        return view;
    }
}
