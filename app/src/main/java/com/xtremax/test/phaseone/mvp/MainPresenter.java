package com.xtremax.test.phaseone.mvp;

import com.xtremax.test.phaseone.models.Post;

import java.lang.ref.WeakReference;

/**
 * Created by E460 on 29/11/2016.
 */

public class MainPresenter implements MainMVP.RequiredPresenterOps, MainMVP.PresenterOps {

    // Layer View reference
    private WeakReference<MainMVP.RequiredViewOps> mView;

    // Layer Model reference
    private MainMVP.ModelOps mModel;

    // COnfiguration change state
    private boolean mIsConfigChanged;

    public MainPresenter(WeakReference<MainMVP.RequiredViewOps> mView) {
        this.mView = mView;
        this.mModel = new MainModel(this);
    }

    @Override
    public void onConfigurationChanged(MainMVP.RequiredViewOps view) {
        mView = new WeakReference<MainMVP.RequiredViewOps>(view);
    }

    @Override
    public void onDestroy(boolean isChangingConfig) {
        mView = null;
        mIsConfigChanged = isChangingConfig;
        if (!isChangingConfig) {
            mModel.onDestroy();
        }
    }

    @Override
    public void createPost(String title, String body) {
        Post post = new Post(0, title, body);
        mModel.insertPost(post);
    }

    @Override
    public void deletePost(Post post) {
        mModel.removePost(post);
    }

    @Override
    public void onPostInserted(Post post) {
        mView.get().showToast("New post added: "+post.getTitle());
    }

    @Override
    public void onPostRemoved(Post post) {
        mView.get().showToast("Post Removed: "+post.getTitle());
    }

    @Override
    public void onError(String message) {
        mView.get().showAlert(message);
    }
}
