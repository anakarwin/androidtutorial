package com.xtremax.test.phaseone.views;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xtremax.test.phaseone.R;
import com.xtremax.test.phaseone.models.UserJsonPlaceholder;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by E460 on 28/11/2016.
 */

@EViewGroup(R.layout.item_user_jsonplaceholder)
public class UserJPViewHolder extends LinearLayout {

    @ViewById(R.id.tv_username)
    TextView mUsername;

    @ViewById(R.id.tv_name)
    TextView mName;

    @ViewById(R.id.tv_email)
    TextView mEmail;

    @ViewById(R.id.tv_address)
    TextView mAddress;

    @ViewById(R.id.tv_phone)
    TextView mPhone;

    @ViewById(R.id.tv_website)
    TextView mWebsite;

    @ViewById(R.id.tv_company)
    TextView mCompany;

    public UserJPViewHolder(Context context) {
        super(context);
    }

    public void bind(UserJsonPlaceholder user) {
        mUsername.setText(user.getUsername());
        mName.setText(user.getName());
        mEmail.setText(user.getEmail());
        mAddress.setText(user.getAddress().toString());
        mPhone.setText(user.getPhone());
        mWebsite.setText(user.getWebsite());
        mCompany.setText(user.getCompany().toString());
    }
}
