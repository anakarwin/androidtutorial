package com.xtremax.test.phaseone.activities;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.xtremax.test.phaseone.R;
import com.xtremax.test.phaseone.fragments.AddressFragment;
import com.xtremax.test.phaseone.fragments.FullnameFragment;
import com.xtremax.test.phaseone.fragments.UserDataFragment;
import com.xtremax.test.phaseone.fragments.UsernameFragment;
import com.xtremax.test.phaseone.helper.UserHelper;
import com.xtremax.test.phaseone.models.User;
import com.xtremax.test.phaseone.mvp.MVPActivity;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<User> users;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        users = UserHelper.generateData();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        ActionBar ab = getSupportActionBar();
        ab.setHomeButtonEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.show();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent = null;

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_go:
                intent = new Intent(this, HttpActivity.class);
                break;
            case R.id.action_camera:
                intent = new Intent(this, CaptureActivity.class);
                break;
            case R.id.action_material:
                intent = new Intent(this, DataSaveActivity.class);
                break;
            case R.id.action_barcode:
                intent = new Intent(this, BarcodeScannerActivity.class);
                break;
            case R.id.action_retrofit:
                intent = new Intent(this, TryRetrofit_.class);
                break;
            case R.id.action_annotations:
                intent = new Intent(this, AnnotationsActivity_.class);
                break;
            case R.id.action_map:
                intent = new Intent(this, MapsActivity.class);
                break;
            case R.id.action_fb:
                intent = new Intent(this, FacebookLogin.class);
                break;
            case R.id.action_new_app:
                intent = new Intent(this, OpenNewAppActivity.class);
                break;
            case R.id.action_mvp:
                intent = new Intent(this, MVPActivity.class);
                break;
            case R.id.action_ar:
                intent = new Intent(this, CameraViewActivity.class);
                break;
        }
        if (intent != null) {
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = UsernameFragment.newInstance(users);
                    break;
                case 1:
                    fragment = FullnameFragment.newInstance(users);
                    break;
                case 2:
                    fragment = AddressFragment.newInstance(users);
                    break;
                case 3:
                    fragment = UserDataFragment.newInstance(users);
                    break;
            }

            return fragment;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Username";
                case 1:
                    return "Fullname";
                case 2:
                    return "Adress";
                case 3:
                    return "Hobbies";
                case 4:
                    return "Skills";
                case 5:
                    return "All Data";
            }
            return null;
        }
    }
}
