package com.xtremax.test.phaseone.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.xtremax.test.phaseone.R;

import java.util.List;

public class OpenNewAppActivity extends AppCompatActivity {

    private Button btnInstagram, btnFacebook, btnInstagramApp, btnFacebookApp
            , btnVideoView, btnYoutubeApp;
    private EditText etYoutube;
    private TextView tvInstagram, tvFacebook, tvVideo;
    private VideoView vvVideo;

    private static final String INSTAGRAM_PACKAGE = "com.instagram.android";
    private static final String FACEBOOK_PACKAGE = "com.facebook.katana";
    private static final String FACEBOOK_DEFAULT = "https://www.facebook.com/yusuf.chena";
    private static final String FACEBOOK_PROTO = "fb://page/yusuf.chena";
    private static final String FACEBOOK_PROTO_NEW = "fb://facewebmodal/f?href=" + FACEBOOK_DEFAULT;
    private static final String INSTAGRAM_DEFAULT = "https://www.instagram.com/noah_site";
    private static final String INSTAGRAM_PROTO = "https://instagram.com/_u/noah_site";
    private static final String VIDEO_LINK = "http://www.androidbegin.com/tutorial/AndroidCommercial.3gp";
    private static final String YOUTUBE_DEFAULT = "https://www.youtube.com/watch?v=1FJHYqE0RDg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_new_app);

        btnInstagram = (Button) findViewById(R.id.btn_instagram);
        btnFacebook = (Button) findViewById(R.id.btn_facebook);
        btnInstagramApp = (Button) findViewById(R.id.btn_instagram_app);
        btnFacebookApp = (Button) findViewById(R.id.btn_facebook_app);
        btnVideoView = (Button) findViewById(R.id.btn_video_view);
        btnYoutubeApp = (Button) findViewById(R.id.btn_youtube_app);
        etYoutube = (EditText) findViewById(R.id.et_youtube_url);
        tvInstagram = (TextView) findViewById(R.id.tv_instagram_url);
        tvFacebook = (TextView) findViewById(R.id.tv_facebook_url);
        tvVideo = (TextView) findViewById(R.id.tv_youtube_url);
        vvVideo = (VideoView) findViewById(R.id.vv_video);

        btnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openBrowser(FACEBOOK_DEFAULT);
            }
        });
        btnInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openBrowser(INSTAGRAM_DEFAULT);
            }
        });
        btnFacebookApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFacebook();
            }
        });
        btnInstagramApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openInstagram();
            }
        });
        btnVideoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadVideo();
            }
        });
        btnYoutubeApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openBrowser(etYoutube.getText().toString());
            }
        });
        tvInstagram.setText(INSTAGRAM_DEFAULT);
        tvFacebook.setText(FACEBOOK_DEFAULT);
        tvVideo.setText(VIDEO_LINK);
        etYoutube.setText(YOUTUBE_DEFAULT);
    }

    public void openInstagram() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(INSTAGRAM_PROTO));
        intent.setPackage(INSTAGRAM_PACKAGE);
        if (isIntentAvaliable(intent)) {
            startActivity(intent);
        }
    }

    public void openFacebook() {
        try {
            int versionCode = getPackageManager().getPackageInfo(FACEBOOK_PACKAGE, 0).versionCode;
            Log.d("debugdebug", "facebook version:"+versionCode);
            if (versionCode >= 3002850) {   // newer version
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(FACEBOOK_PROTO_NEW)));
            } else {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(FACEBOOK_PROTO)));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(FACEBOOK_DEFAULT)));
        }
    }

    public void openBrowser(String url) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    }

    public void openYoutube() {
//        openBrowser();
    }

    private void loadVideo() {
        Log.d("debugdebug", "dipanggil");
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(vvVideo);
        vvVideo.setVideoURI(Uri.parse(VIDEO_LINK));
        vvVideo.setMediaController(mediaController);
        vvVideo.requestFocus();
        vvVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                vvVideo.start();
            }
        });
    }

    private boolean isIntentAvaliable(Intent intent) {
        List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }
}
