package com.xtremax.test.phaseone.mvp;

import com.xtremax.test.phaseone.models.Post;

/**
 * Created by E460 on 29/11/2016.
 */

public interface MainMVP {

    /**
     * View Mandatory methods
     * P -> V
     */
    interface RequiredViewOps {
        void showToast(String msg);
        void showAlert(String msg);
    }

    /**
     * Operations offered from P to V
     * V -> P
     */
    interface PresenterOps {
        void onConfigurationChanged(RequiredViewOps view);
        void onDestroy(boolean isChangingConfig);
        void createPost(String title, String body);
        void deletePost(Post post);
    }

    /**
     * operations offered from model to presenter
     * M -> P
     */
    interface RequiredPresenterOps {
        void onPostInserted(Post post);
        void onPostRemoved(Post post);
        void onError(String message);
    }

    /**
     * Model operations offered to Presenter
     * P -> M
     */
    interface ModelOps {
        void insertPost(Post post);
        void removePost(Post post);
        void onDestroy();
    }
}
