package com.xtremax.test.phaseone.network;

import com.xtremax.test.phaseone.models.UserRetrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by E460 on 23/11/2016.
 */

public interface UserEndpointInterface {
    @GET("{username}")
    Call<UserRetrofit> getUser(@Path("username") String username);

    @GET("all")
    Call<List<UserRetrofit>> getUsers(@Query("sort") String sort);

    @POST("new")
    Call<UserRetrofit> createuser(@Body UserRetrofit user);
}
