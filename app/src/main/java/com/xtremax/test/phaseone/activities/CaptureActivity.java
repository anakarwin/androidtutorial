package com.xtremax.test.phaseone.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.xtremax.test.phaseone.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CaptureActivity extends AppCompatActivity {

    private Button btnCapture;
    private ImageView ivResult;
    private TextView tvReport;

    private String mCurrentPhotoPath;

    private static final int REQ_CODE_CAPTURE = 1;
    private static final int CAMERA_PERMISSION_CODE = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture);

        btnCapture = (Button) findViewById(R.id.btn_capture);
        ivResult = (ImageView) findViewById(R.id.iv_result);
        tvReport = (TextView) findViewById(R.id.tv_report);

        btnCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Intent> intentList = new ArrayList<>();
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                Log.d("debugdebug","capture clicked");
                if (intent.resolveActivity(getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = creatingImageFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (photoFile != null) {
                        Uri photoURI = FileProvider.getUriForFile(getBaseContext(),
                                "com.xtremax.test.phaseone.FileProvider", photoFile);
                        intent.putExtra("return-data", true);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
//                        startActivityForResult(intent, REQ_CODE_CAPTURE);
                    }
                }

                intentList = addIntentToList(getBaseContext(), intentList, intent);
                intentList = addIntentToList(getBaseContext(), intentList, galleryIntent);
                Intent chooserIntent = null;
                if (intentList.size() > 0) {
                    chooserIntent = Intent.createChooser(intentList.remove(intentList.size() - 1),
                            "Pick Image");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                            intentList.toArray(new Parcelable[]{}));
                }
                startActivityForResult(chooserIntent, REQ_CODE_CAPTURE);
            }
        });

        requestCameraPermission();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission granted
                // do whatever you want
            } else {
                // permission denied
                // do whatever you want
            }
        }
    }

    private void requestCameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        ActivityCompat.requestPermissions(this,
                new String[] {Manifest.permission.CAMERA}, CAMERA_PERMISSION_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_CODE_CAPTURE && resultCode == RESULT_OK) {
            Bitmap bitmap = null;
            Uri uri;
            if (data == null) {
                File file = new File(mCurrentPhotoPath);
                uri = Uri.fromFile(file);
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                tvReport.setText("Saved in "+mCurrentPhotoPath);
                galleryAddPic();
            } else {
                uri = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            ivResult.setImageBitmap(bitmap);

//            File file = new File(mCurrentPhotoPath);
//            Uri  uri = Uri.fromFile(file);
//            try {
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
//                ivResult.setImageBitmap(bitmap);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }

        }
    }

    private File creatingImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyMMdd_HHmmss").format(new Date());
        String imageFileName = "PhaseOne_" + timeStamp;
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentURI = Uri.fromFile(f);
        mediaScanIntent.setData(contentURI);
        this.sendBroadcast(mediaScanIntent);
    }

    private static List<Intent> addIntentToList(Context context, List<Intent> list, Intent intent) {
        List<ResolveInfo> resInfos = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo res : resInfos) {
            String packageName = res.activityInfo.packageName;
            Intent targetedIntent = new Intent(intent);
            targetedIntent.setPackage(packageName);
            list.add(targetedIntent);
        }
        return list;
    }
}
