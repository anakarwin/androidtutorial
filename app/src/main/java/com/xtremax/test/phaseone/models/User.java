package com.xtremax.test.phaseone.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by E460 on 22/11/2016.
 */

public class User implements Parcelable{

    private int id;
    private String username;
    private String fullname;
    private String address;
    private List<String> hobbies;
    private List<String> skills;

    public User() {
    }

    public User(int id, String username, String fullname, String address, List<String> hobbies, List<String> skills) {
        this.id = id;
        this.username = username;
        this.fullname = fullname;
        this.address = address;
        this.hobbies = hobbies;
        this.skills = skills;
    }

    // getter and setter

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<String> getHobbies() {
        return hobbies;
    }

    public void setHobbies(List<String> hobbies) {
        this.hobbies = hobbies;
    }

    public List<String> getSkills() {
        return skills;
    }

    public void setSkills(List<String> skills) {
        this.skills = skills;
    }

    // parcelable methods

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(id);
        parcel.writeString(username);
        parcel.writeString(fullname);
        parcel.writeString(address);
        parcel.writeStringList(hobbies);
        parcel.writeStringList(skills);
    }

    public static Parcelable.Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel parcel) {
            return new User(parcel);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    protected User(Parcel in) {
        id = in.readInt();
        username = in.readString();
        fullname = in.readString();
        address = in.readString();
        in.readStringList(hobbies);
        in.readStringList(skills);
    }
}
