package com.xtremax.test.phaseone.activities;

import android.os.Parcelable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.xtremax.test.phaseone.R;
import com.xtremax.test.phaseone.fragments.UserJsonPlaceholderFragment;
import com.xtremax.test.phaseone.fragments.UserJsonPlaceholderFragment_;
import com.xtremax.test.phaseone.models.UserJsonPlaceholder;
import com.xtremax.test.phaseone.models.UserRetrofit;
import com.xtremax.test.phaseone.network.UserEndpointInterface;
import com.xtremax.test.phaseone.network.UserJPInterface;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@EActivity(R.layout.activity_annotations)
public class AnnotationsActivity extends AppCompatActivity implements Callback<ArrayList<UserJsonPlaceholder>> {

    //    private final static String BASE_URL = "http://10.0.2.2:5000/user/";
    private final static String DOWNLOAD_URL = "http://10.0.2.2:5000/";
    private final static String BASE_URL = "https://jsonplaceholder.typicode.com/";

    private Retrofit mrRetrofit;

    @Extra
    ArrayList<UserJsonPlaceholder> users;

    void getData() {
        UserJPInterface userJPInterface = mrRetrofit.create(UserJPInterface.class);
        Call<ArrayList<UserJsonPlaceholder>> call = userJPInterface.getUsers();
        call.enqueue(this);
    }

    @AfterViews
    void prepareRetrofit() {
        mrRetrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        getData();
    }

    @Override
    public void onResponse(Call<ArrayList<UserJsonPlaceholder>> call, Response<ArrayList<UserJsonPlaceholder>> response) {
        Log.d("debugdebug", "body: "+response.body());
        if (response.code() == 200) {
            Log.d("debugdebug", "body: "+response.body());
            users = response.body();
            UserJsonPlaceholderFragment fragment = new UserJsonPlaceholderFragment_().builder()
                    .users(users).build();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment)
                    .commit();
        }
    }

    @Override
    public void onFailure(Call<ArrayList<UserJsonPlaceholder>> call, Throwable t) {
        Toast.makeText(this, "Failure", Toast.LENGTH_SHORT).show();
    }
}
