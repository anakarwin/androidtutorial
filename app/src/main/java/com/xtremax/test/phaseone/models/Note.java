package com.xtremax.test.phaseone.models;

import android.os.Bundle;
import android.util.JsonReader;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by E460 on 22/11/2016.
 */

public class Note {
    private int id;
    private String text;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static Note fromJson(String jsonString) {
        Note note = new Note();
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            note.setId(jsonObject.getInt("id"));
            note.setText(jsonObject.getString("text"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return note;
    }
}
