package com.xtremax.test.phaseone.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.xtremax.test.phaseone.R;
import com.xtremax.test.phaseone.adapters.UsernameAdapter;
import com.xtremax.test.phaseone.helper.UserHelper;
import com.xtremax.test.phaseone.models.User;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UsernameFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UsernameFragment extends Fragment {

    private List<User> users;

    public UsernameFragment() {
        // Required empty public constructor
    }

    public static UsernameFragment newInstance(List<User> users) {
        UsernameFragment fragment = new UsernameFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(UserHelper.ARG_USERS, (ArrayList) users);
        fragment.setArguments(args);
        return fragment;
    }

    // parcelable

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            users = getArguments().getParcelableArrayList(UserHelper.ARG_USERS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_username, container, false);
        ListView lvContainer = (ListView) view.findViewById(R.id.lv_container);
        UsernameAdapter adapter = new UsernameAdapter(getContext(),
                android.R.layout.simple_list_item_1, users);
        lvContainer.setAdapter(adapter);
        return view;
    }

}
